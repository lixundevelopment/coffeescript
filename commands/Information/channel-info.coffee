Command = require "../../structures/Command"
{ MessageEmbed } = require "discord.js"

class ChannelInfo extends Command
  constructor: (args...) ->
    super args...,
      name: 'Channel-info',
      description: 'Show information on a channel',
      extendedHelp: 'Show information on the current channel or a mentioned channel',
      aliases: ["channel", "channelinfo"],
      cooldown: 3,
      usage: '[Channel]',
      botPermissions: ['SEND_MESSAGES'],
      category: 'Information'

  run: (msg) ->

    Channel = msg.mentions.channels.first() || msg.channel

    Embed = new MessageEmbed()
    .setTitle('Channel Information')
    .setColor("ORANGE")
    .addField('ID', Channel.id, true)
    .addField('Name', Channel.name, true)
    .addField('Mention', "`<##{Channel.id}>`", true)
    .addField('Category', Channel.parent.name, true)

    msg.channel.send(Embed)

module.exports = ChannelInfo