Discord = require "discord.js"
Command = require "../../structures/Command"

class UserInfo extends Command
  constructor: (...args) ->
    super ...args, 
      description: "Show information on a user",
      aliases: ['user'],
      usage: '[Member]',
      botPermissions: ['SEND_MESSAGES'],
  
  run: (msg, args) ->
    
    Member = message.mentions.members.first()

    ErrorEmbed = new Discord.MessageEmbed()
    .setTitle('Missing arguments')
    .setDescription(':x: You must provide a role')
    .setColor("RED")
    .setFooter(msg.author.username, msg.author.displayAvatarURL( dynamic: true ))

    return msg.channel.send(ErrorEmbed) if not Member

    Embed = new Discord.MessageEmbed()
    .setTitle("Showing information on #{Member.user.username}")
    .setColor(Member.displayHexColor || 'RANDOM')
