Command = require "../../structures/Command"
{ MessageEmbed } = require "discord.js"
moment = require "moment"

class RoleInfo extends Command
  constructor: (args...) ->
    super args...,
      name: 'Role-info',
      description: 'Show information on a role',
      extendedHelp: 'Show information a mentioned role',
      aliases: ['roleinfo', 'role'],
      cooldown: 3,
      category: 'Information',
      botPermissions: ['SEND_MESSAGES'],

  run: (msg) ->

    Role = msg.mentions.roles.first()

    ErrorEmbed = new MessageEmbed()
    .setTitle('Missing arguments')
    .setDescription(':x: You must provide a role')
    .setColor("RED")
    .setFooter(msg.author.username, msg.author.displayAvatarURL( dynamic: true ))

    return msg.channel.send(ErrorEmbed) if not Role

    Hoist

    Mentionable

    if Role.hoist is true then Hoist = 'Yes' else Hoist = 'No'

    if Role.mentionable is true then Mentionable = 'Yes' else Mentionable = 'No'

    Embed = new MessageEmbed()
    .setTitle('Role Information')
    .setColor(Role.hexColor || "ORANGE")
    .addField('Role Name', Role.name, true)
    .addField('Role ID', Role.id, true)
    .addField('Role Mention', "`<@&#{Role.id}>`", true)
    .addField('Colour', Role.hexColor, true)
    .addField('Members', Role.members.size, true)
    .addField('Created At', moment(Role.createdAt).format('LLLL'), true)
    .addField('Position (from bottom)', "#{Role.position}", true)
    .addField('Hoisted', "#{Hoist}", true)
    .addField('Mentionable', "#{Mentionable}", true)

    msg.channel.send(Embed)

module.exports = RoleInfo