Discord = require "discord.js"
{ MessageEmbed } = require "discord.js"
Command = require "../../structures/Command"

class Suggest extends Command
  constructor: (args...) ->
    super args...,
      description: "Make a suggestion to the server!",
      cooldown: 10,
      usage: "<Suggestion>",

  run: (msg, args) ->

   errorembed = new MessageEmbed()
   .setTitle('Missing arguments')
   .setDescription(':x: You must provide a suggestion')
   .setColor("RED")

   return msg.channel.send(errorembed) if not args.length

   msg.delete()

   embed = new MessageEmbed()
   .setTitle('Suggestion!')
   .setColor("ORANGE")
   .setDescription("#{msg.author.username} (#{msg.author.id})")
   .addField('Suggestion!', args.join(' '))

   msg.channel.send(embed)

module.exports = Suggest