// Generated by CoffeeScript 2.5.1
(function() {
  var Collection, Store, klaw, path;

  klaw = require("klaw");

  path = require("path");

  ({Collection} = require("discord.js"));

  Store = class Store extends Collection {
    constructor(client, name) {
      super();
      this.client = client;
      this.name = name;
      this.dir = `${path.dirname(require.main.filename)}${path.sep}${name}`;
    }

    set(piece) {
      var exists;
      exists = this.get(piece.name);
      if (exists) {
        this.delete(piece.name);
      }
      super.set(piece.name, piece);
      return piece;
    }

    delete(key) {
      var exists;
      exists = this.get(key);
      if (!exists) {
        false;
      }
      return super.delete(key);
    }

    load(file) {
      var filepath, piece;
      filepath = path.join(this.dir, file);
      piece = this.set(new (require(filepath))(this.client, {
        path: file,
        name: path.parse(filepath).name
      }));
      delete require.cache[filepath];
      return piece;
    }

    loadFiles() {
      return new Promise((resolve, reject) => {
        return klaw(this.dir).on("data", (item) => {
          var err;
          if (!item.path.endsWith(".js") && !item.path.endsWith(".coffee")) {
            return;
          }
          try {
            return this.load(path.relative(this.dir, item.path));
          } catch (error) {
            err = error;
            return reject(err);
          }
        }).on("error", (err) => {
          return reject(err);
        }).on("end", () => {
          return resolve(this.size);
        });
      });
    }

  };

  module.exports = Store;

}).call(this);
