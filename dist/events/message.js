// Generated by CoffeeScript 2.5.1
(function() {
  var Event, Message, Permissions;

  Event = require("../structures/Event");

  ({Permissions} = require("discord.js"));

  Message = class Message extends Event {
    constructor(...args) {
      var cb, titleCase;
      super(...args);
      this.prefix = this.client.config.prefix;
      this.ratelimits = new Map();
      // Uppercase the first letter kick => Kick
      titleCase = (key) => {
        return key[0].toUpperCase() + key.slice(1);
      };
      // CoffeeScript was a little bit of headscratching here
      // So i ended up splitting these into seperate functions.
      cb = (obj, key) => {
        obj[key] = key.toLowerCase().split("_").map(titleCase).join(" ");
        return obj;
      };
      // Build a friendly permission list, e.g KICK_MEMBERS => Kick Members
      this.friendlyPerms = Object.keys(Permissions.FLAGS).reduce(cb, {});
    }

    async run(msg) {
      var args, cmd, command, rl;
      // Ignore bots and webhooks
      if (msg.webhookID || msg.author.bot) {
        return;
      }
      // Ensure the bot is in the member cache.
      if (msg.guild && !msg.guild.me) {
        await msg.guild.members.fetch(this.client.user);
      }
      // Remind the user the prefix if they mentioned the bot.
      if (msg.content === this.client.user.toString() || msg.guild && msg.content === msg.guild.me.toString()) {
        return msg.channel.send(`Hi, my prefix is \`${this.prefix}\``);
      }
      if (!msg.content.startsWith(this.prefix)) {
        return;
      }
      args = msg.content.slice(this.prefix.length).trim().split(/ +/g);
      cmd = args.shift().toLowerCase();
      command = this.client.commands.get(cmd);
      if (!command) {
        return this.client.emit("commandUnknown", msg, cmd);
      }
      // Check cooldown.
      rl = this.ratelimit(msg, command);
      if (typeof rl === "string") {
        return msg.channel.send(rl);
      }
      if (command.ownerOnly && msg.author.id !== this.client.config.owner) {
        return msg.channel.send("Sorry but that command is for the owner only.");
      }
      if (command.nsfw && !msg.channel.nsfw) {
        return msg.channel.send("That command can only be used in NSFW channels.");
      }
      if (command.guildOnly && !msg.guild) {
        return msg.channel.send("That command can only be used in a server.");
      }
      if (!command.enabled && msg.author.id !== this.client.config.owner) {
        return msg.channel.send("That command has been disabled by the owner.");
      }
      // Verify the member is in cache if the command is a guild only command.
      if (command.guildOnly && !msg.member) {
        await msg.guild.members.fetch(msg.author);
      }
      if (!(await this.checkPerms(msg, command))) {
        return;
      }
      msg.channel.startTyping();
      await command._run(msg, args);
      return msg.channel.stopTyping();
    }

    ratelimit(msg, cmd) {
      var cooldown, difference, ratelimits;
      if (msg.author.id === this.client.config.owner) {
        return;
      }
      if (cmd.cooldown === 0) {
        return;
      }
      cooldown = cmd.cooldown * 1000;
      ratelimits = this.ratelimits.get(msg.author.id) || {};
      if (!ratelimits[cmd.name]) {
        ratelimits[cmd.name] = Date.now() - cooldown;
      }
      difference = Date.now() - ratelimits[cmd.name];
      if (difference < cooldown) {
        return msg.channel.send(`You can run this command again in **${Math.round((cooldown - difference) / 1000)} seconds**`);
      } else {
        ratelimits[cmd.name] = Date.now();
        this.ratelimits.set(msg.author.id, ratelimits);
        return true;
      }
    }

    async checkPerms(msg, cmd) {
      var bot, user;
      if (msg.channel.type === "dm") {
        return true;
      }
      user = msg.author.id === this.client.config.owner ? [] : msg.channel.permissionsFor(msg.author).missing(cmd.userPermissions);
      if (user.length > 0) {
        await msg.channel.send(`You do not have the following permission${user.length > 1 ? "s" : ""} to run this command: \`${user.map((p) => {
          return this.friendlyPerms[p];
        }).join(", ")}\``);
        return false;
      }
      bot = msg.channel.permissionsFor(this.client.user).missing(cmd.botPermissions);
      if (bot.length > 0) {
        await msg.channel.send(`Hey! I need the following permission${bot.length > 1 ? "s" : ""} to do that: \`${bot.map((p) => {
          return this.friendlyPerms[p];
        }).join(", ")}\``);
        return false;
      }
      return true;
    }

  };

  module.exports = Message;

}).call(this);
